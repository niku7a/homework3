fun main(): Unit {

    val age: Byte = 25
    val myCharacter: Char = 'p'
    val s: Short = -100
    val integer: Int = 2000
    val l: Long = 2000L

    val pi: Float = 3.14F
    val d: Double = 2.29

    val bool: Boolean = false

    if (age < 10) {
        println("You can't watch this movie")
    } else if (age < 14) {
        println("You can watch this movie with a parent")
    } else {
        println("You can watch this movie")
    }

    val p = sayHello("Jemali")
    println(p)
    sayHello("Nazi")
    sayHello("Ale")

}

fun sayHello(name: String): String {
    return "Hello $name!"
}